package com.example.leakcanarytask

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Thread(LeakRunnable()).start()
    }

    inner class LeakRunnable : Runnable {
        override fun run() {
            var i = 0
            while (true) {
                runOnUiThread { findViewById<TextView>(R.id.text).text = i.toString() }
                i++
                Thread.sleep(1000)
            }
        }

    }
}